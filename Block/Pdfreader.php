<?php


namespace M21\Pdfreader\Block;

class Pdfreader extends \Magento\Framework\View\Element\Template
{

    protected $_registry;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Framework\Registry $registry,
        array $data = []
    )
    {
        parent::__construct($context, $data);
        $this->_registry = $registry;
    }

    /**
     * @return string
     */
    public function Pdfreader()
    {

        return $this->getCurrentProduct()->getPdfReader();
    }

    public function _prepareLayout()
    {
        return parent::_prepareLayout();
    }

    public function getCurrentCategory()
    {
        return $this->_registry->registry('current_category');
    }

    public function getCurrentProduct()
    {
        return $this->_registry->registry('current_product');
    }
}
